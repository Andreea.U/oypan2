<!doctype html>
<html lang="en">

<head>
    <title>Proascon</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<?php
require('./lib/functions.php');
?>
<body>
<body>
    <header>
        <div class="container-fluid p-0">
            <nav class="navbar navbar-expand-lg">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav2">
                    <!-- the aligning to the right -->
                    <div class="mr-auto"></div>
                    <ul class="navbar-nav ">
                        <li class="nav-item active">
                            <a class="nav-link" href="index.html">Acasa <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="desprenoi.php">Despre noi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="servicii.php">Servicii</a>
                        </li>
                        <li class="nav-item">
                            <!-- <li class="nav-item">
                            <a class="nav-link" href="referinte.html">Referinte <span
                                    class="sr-only">(current)</span></a>
                        </li> -->
                        <li class="nav-item">
                            <a class="nav-link" href="contact.html">Contact <span class="sr-only">(current)</span></a>
                        </li>

                    </ul>
                </div>
            </nav>

        </div>
        <?php
                $result = queryResult('
                SELECT * FROM despre_noi
                where id=2
                ')
              ?>
        <div class="container">
            <div class="serviciiImg">
                <img id="service1" class="img-fluid" alt="Responsive image">
            </div>
            <section>
                <div class="text2">
            <?php
                if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) { 
          ?>
          <p><?php echo $row["descriere"] ?></p>
        <?php   
        }
    }  else {
        `echo "0 results`;
      }
      ?>
                </div>
            </section>
        </div>
    </header>
    <main>

    </main>
    <footer>
        <div class="footer">
            <p>contact@proascon.ro</p>
            <p>0259418489 | 072272378</p>

        </div>

    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="main.js"></script>
</body>

</html>